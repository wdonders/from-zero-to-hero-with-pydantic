# From Zero to Hero with Pydantic parsing
The contents of this repository were submitted but rejected for a talk for PyData Eindhoven 2022.
The contents are sponsored by 42Analytics.

![42Analytics](./42Analytics-logo-horizontal.svg)

This repository contains examples for how you can use `pydantic` to parse and validate data obtained from an external source, such as a REST API.
Examples show how to use pydantic to:
- define a data model
- deal with varied data types in fields
- deal with missing data
- deal with unwanted data field names
- validate values
- deal with pesky dates

You will learn `pydantic` data models, type annotations, fields, constrained types and validators so you can configure complex data models such as the one below.

```python
from pydantic import BaseModel, Field, NonNegativeInt, constr, validator
import datetime
from typing import ClassVar

class Name(BaseModel):
    first: str | None
    last: constr(min_length=1)


class Person(BaseModel):
    id: NonNegativeInt
    is_active: bool
    name: Name
    company: str | None
    birth_date: datetime.date

    # ClassVar types refer to class variables, i.e. these are not fields for class instances
    TODAY: ClassVar[datetime.date] = datetime.date.today()
    EARLIEST_ALLOWED_BIRTH_DATE: ClassVar[datetime.date] = datetime.date(1900, 1, 1)
    LATEST_ALLOWED_BIRTH_DATE: ClassVar[datetime.date] = TODAY

    @validator("birth_date", pre=True)
    def birth_date_must_be_iso8601(cls, value):
        """Convert the %m-%d-%Y string to %Y-%m-%d before actual `datetime.date` validation is run"""
        birth_date = datetime.datetime.strptime(value, "%d-%m-%Y")
        return birth_date.strftime("%Y-%m-%d")

    @validator("birth_date")
    def birth_date_not_before(cls, value):
        """Validate that birth_date value is not before EARLIEST_ALLOWED_BIRTH_DATE"""
        if value < cls.EARLIEST_ALLOWED_BIRTH_DATE:
            raise ValueError(
                f"Birth date {value} precedes earliest allowed date {cls.EARLIEST_ALLOWED_BIRTH_DATE}"
            )
        return value

    @validator("birth_date")
    def birth_date_not_after(cls, value):
        """Validate that birth date value does not exceed LATESTS_ALLOWED_BIRTH_DATE"""
        if value > cls.LATEST_ALLOWED_BIRTH_DATE:
            raise ValueError(
                f"Birth date {value} exceeds latest allowed date {cls.LATEST_ALLOWED_BIRTH_DATE}"
            )
        return value


class Data(BaseModel):
    people: list[Person]
```

# Examples
The notebook `From Zero to Hero with Pydantic.ipynb` contains examples on the usage of Pydantic in a variety of cases.
It uses generated sample data that is stored in the `data` directory.

You can simply read the notebook, or run it interactively (which has some installation requirements).

# Install requirements
To run the Notebook interactively, you must first install the requirements.
It is recommended to do this in a virtual environment to separate it from other projects.

**Note**: To correctly pull the example data, it is required that you have [`git-lfs`](https://git-lfs.com/) installed!

## For Linux users
Setup a virtual environment and install all requirements:

```bash
$ make venv
```

## For Windows users:
It is recommended you first create a virtual environment before installing requirements.

```cmd
# python -m venv venv
# venv/bin/activate.bat
# pip install -r requirements.txt
```
