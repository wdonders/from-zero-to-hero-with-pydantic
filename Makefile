.phony: jupyter

.ONESHELL:

.SHELLFLAGS := -euo pipefail -c
SHELL := bash

venv:
	python -m venv venv
	source venv/bin/activate
	pip install -r requirements.txt

dev: venv
	source venv/bin/activate
	pip install -r requirements-dev.txt
	pre-commit install

lint:
	pre-commit run --all-files

jupyter: venv
	source venv/bin/activate
	jupyter-lab
